import 'react-native-gesture-handler';
import React from 'react';
import { NavigationContainer } from '@react-navigation/native';
import { StyleSheet, Text, View } from 'react-native';
import { createStackNavigator } from '@react-navigation/stack';
import HomeScreen from './screen/HomeScreen';
import TaskScreen from './screen/TaskScreen';
import SecondScreen from './screen/SecondScreen';
import ThirdScreen from './screen/ThirdScreen';
import NonProfitDetailsScreen from './screen/NonProfitDetailsScreen';
import SuccessScreen from './screen/SuccessScreen';
import WalletScreen from './screen/WalletScreen';
import ComponentsDemo from './screen/ComponentsDemo';
import SavedOffersScreen from './screen/SavedOfferScreen';


const Stack = createStackNavigator();

export default function App() {
    return (
        <NavigationContainer>
            <Stack.Navigator initialRouteName="Home">
                <Stack.Screen name="Home"
                    component={HomeScreen}
                    options={{
                        title: 'Home Screen',
                        headerStyle: {
                            backgroundColor: '#FFFFFF',
                        },
                        headerTitileStyle: {
                            fontWeight: 'normal',
                        },
                        headerTintColor: '#404040',
                        headerBackTitle: ' '
                    }}
                />
                <Stack.Screen name="Task" component={TaskScreen}
                    options={{
                        title: 'Contribute Walgreens Cash',
                        headerStyle: {
                            backgroundColor:'#FFFFFF',
                        },
                        headerTitileStyle: {
                            fontWeight: 'bold',
                        },
                        headerTintColor:'#404040',
                        headerBackTitle: ' '
                    }}
                />
                <Stack.Screen name="Second" component={SecondScreen}
                    options={{
                        title: 'Contribute Walgreens Cash',
                        headerStyle: {
                            backgroundColor:'#FFFFFF',
                        },
                        headerTitileStyle: {
                            fontWeight: 'bold',
                        },
                        headerTintColor:'#404040',
                        headerBackTitle: ' '
                    }}
                />
                <Stack.Screen name="Third" component={ThirdScreen}
                    options={{
                        title: 'Contribute Walgreens Cash',
                        headerStyle: {
                            backgroundColor:'#FFFFFF',
                        },
                        headerTitileStyle: {
                            fontWeight: 'bold',
                        },
                        headerTintColor:'#404040',
                        headerBackTitle: ' '
                    }}
                />
                <Stack.Screen name="NonProfit" component={NonProfitDetailsScreen}
                    options={{
                        title: 'Non-Profit Details',
                        headerStyle: {
                            backgroundColor:'#FFFFFF',
                        },
                        headerTitileStyle: {
                            fontWeight: 'bold',
                        },
                        headerTintColor:'#404040',
                        headerBackTitle: ' '
                    }}
                />
                <Stack.Screen name="Success" component={SuccessScreen}
                    options={{
                        title: 'Success',
                        headerStyle: {
                            backgroundColor:'#FFFFFF',
                        },
                        headerTitileStyle: {
                            fontWeight: 'bold',
                        },
                        headerTintColor:'#404040',
                        headerBackTitle: ' '
                    }}
                /> 
                <Stack.Screen name="WalletScreen" component={WalletScreen}
                    options={{
                        title: 'WalletScreen',
                        headerStyle: {
                            backgroundColor:'#FFFFFF',
                        },
                        headerTitileStyle: {
                            fontWeight: 'bold',
                        },
                        headerTintColor:'#404040',
                        headerBackTitle: ' '
                    }}
                />
                <Stack.Screen name="Components" component={ComponentsDemo}
                    options={{
                        title: 'WalletScreen',
                        headerStyle: {
                            backgroundColor:'#FFFFFF',
                        },
                        headerTitileStyle: {
                            fontWeight: 'bold',
                        },
                        headerTintColor:'#404040',
                        headerBackTitle: ' '
                    }}
                />
                <Stack.Screen name="SavedOffersScreen" component={SavedOffersScreen}
                    options={{
                        title: 'Saved Offers',
                        headerStyle: {
                            backgroundColor:'#FFFFFF',
                        },
                        headerTitileStyle: {
                            fontWeight: 'bold',
                        },
                        headerTintColor:'#404040',
                        headerBackTitle: ' '
                    }}
                />
            </Stack.Navigator>
        </NavigationContainer>
    );
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#fff',
        alignItems: 'center',
        justifyContent: 'center',
    },
});