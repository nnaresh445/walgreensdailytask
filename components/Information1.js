import React from 'react';
import { StyleSheet, Text, View} from 'react-native';
import * as CustomStyles from '../constants/style';
import * as Fonts from '../constants/Font';
import * as Dimensions from '../constants/Dimension';
import * as Color from '../constants/Color';

const font = Fonts.Font;
const dimension = Dimensions.Dimension;
const color = Color.Color;

const Information1 = (props) => {
    return (
        <View style={CustomStyles.styles.card}>
            <View style={style.informationContainer}>
                <Text style={style.title}>{props.title}</Text>
                <Text style={style.text}>{prop.text}</Text>
            </View>
        </View>
    );
};

const style = StyleSheet.create({
    informationContainer: {
        padding: dimension.Sixteen
    },

    textContainer: {
        marginBottom: dimension.Eight,
    },

    title: {
        fontSize: font.Title3,
        color: color.GrayAAA,
        fontWeight: 'bold',
    },

    text: {
        fontSize: font.Body,
        color: color.GrayAAA,
    }
});

export default Information1;;