import React from 'react';
import { StyleSheet, Text, View, Image, Button } from 'react-native';
import * as CustomStyles from '../constants/style';
import * as Fonts from '../constants/Font';
import * as Dimensions from '../constants/Dimension';
import * as Color from '../constants/Color';
import { TouchableOpacity } from 'react-native-gesture-handler';

const font = Fonts.Font;
const dimension = Dimensions.Dimension;
const color = Color.Color;

const ProductCoupon = ({bodyTitle, subText, detailsText, buttonText, detailsAction, buttonAction}) => {
    return (
        <View style={styles.card}>
            <View style={styles.informationContainer}>
                <View style={styles.chevronRightContainer}>
                    <Image style={{ width: 70, height: 70, alignItems: 'center' }}
                        source={require('../assets/walgreens_logo.png')} />
                </View>
                <View style={{alignItems:'flex-start', flexWrap:'wrap', justifyContent:'flex-start', paddingBottom: 10}}>
                <Text style={styles.bodyTitle}>{bodyTitle}</Text>
                <Text style={styles.subText}>{subText}</Text>
                <TouchableOpacity onPress={detailsAction}>
                    <Text style={{fontWeight:'bold', textAlign:'left', fontSize: 16}}>
                    {detailsText}
                    </Text>
                </TouchableOpacity>
                </View>
                <TouchableOpacity onPress={buttonAction}>
                    <Text style={{fontWeight:'bold', textAlign:'center', fontSize: 18}}>
                    {buttonText}
                    </Text>
                </TouchableOpacity>
            </View>
        </View>
    );
};

const styles = StyleSheet.create({
    card: {
        shadowColor: 'black',
        shadowOffset: { width: 0, height: 2 },
        shadowRadius: 6,
        shadowOpacity: 0.26,
        backgroundColor: 'white',
        elevation: 5,
        borderRadius: 15,
        width: 160
    },

    informationContainer: {
        padding: dimension.Sixteen,
    },

    textContainer: {
        marginBottom: dimension.Eight,
    },

    bodyTitle: {
        justifyContent: 'flex-start',
        fontSize: font.Body,
        color: color.GrayAAA,
        fontWeight: 'bold',
    },

    subText: {
        fontSize: font.Subhead,
        color: color.GrayAA,
        marginBottom: 5
    },

    chevronRightContainer: {
        justifyContent: 'center',
        alignItems: 'center',
        marginBottom: 10
    }
});

export default ProductCoupon;