import React from 'react';
import { StyleSheet, Text, View } from 'react-native';

const StaticView = (props) => {
    return (
        <View style={styles.yourImpactCardContainer} >
            <View style={{ flex: 1, padding: 15, borderBottomLeftRadius: 15, borderBottomRightRadius: 15, overflow: 'hidden' }} >
                <Text style={styles.otherHeaderTitle}>{props.title1}</Text>
                <Text style={{ fontSize: 16 }}>{props.title2}</Text>
            </View>
        </View>
    );
};

const styles = StyleSheet.create({
    otherHeaderTitle: {
        fontSize: 19,
        paddingBottom: 5,
        color: '#404040',
        fontWeight: 'bold',
    },
    yourImpactCardContainer: {
        shadowColor: 'black',
        shadowOffset: { width: 0, height: 2 },
        shadowRadius: 6,
        shadowOpacity: 0.26,
        backgroundColor: '#DCDCDC',
        elevation: 5,
        borderRadius: 15,
        borderWidth: 1,
    },
});

export default StaticView;