import React from 'react';
import { StyleSheet, Text, View } from 'react-native';

const SplitContentView1 = (props) => {
    return (
        <View style={styles.yourImpactCardContainer} >
            <View style={{ flex: 1, padding: 40, justifyContent: 'center', backgroundColor: '#C0C0C0', borderTopLeftRadius: 15, borderTopRightRadius: 15, overflow: 'hidden' }} >
            </View>
            <View style={{ flex: 1, padding: 15, borderBottomLeftRadius: 15, borderBottomRightRadius: 15, overflow: 'hidden' }} >
                <Text style={styles.otherHeaderTitle}>{props.title1}</Text>
                <Text style={{ fontSize: 16 }}>{props.title2}</Text>
            </View>
        </View>
    );
};

const styles = StyleSheet.create({
    otherHeaderTitle: {
        fontSize: 27,
        paddingBottom: 5,
        color: '#404040',
        fontWeight: 'bold',
    },
    yourImpactCardContainer: {
        shadowColor: 'black',
        shadowOffset: { width: 0, height: 2 },
        shadowRadius: 6,
        shadowOpacity: 0.26,
        backgroundColor: 'white',
        elevation: 5,
        borderRadius: 15,
    },
});

export default SplitContentView1;