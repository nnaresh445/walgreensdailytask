import React from 'react';
import { StyleSheet, Text, View} from 'react-native';
import * as CustomStyles from '../constants/style';
import * as Fonts from '../constants/Font';
import * as Dimensions from '../constants/Dimension';
import * as Color from '../constants/Color';

const font = Fonts.Font;
const dimension = Dimensions.Dimension;
const color = Color.Color;

const ProgressInformation = (props) => {
    return (
        <View style={CustomStyles.styles.card}>
            <Text style={style.otherSubHeaderTitle}>{props.header} </Text>
            <View style={style.progressBarContainer}>
                <View style={style.innerProgressBarContainer} />
            </View>
            <Text style={style.monthlyGoalText}>{props.message} </Text>
        </View>
    );
};

const style = StyleSheet.create({
    informationContainer: {
        padding: dimension.Sixteen
    },

    textContainer: {
        marginBottom: dimension.Eight,
    },

    title: {
        fontSize: font.Title2,
        color: color.GrayAAA,
        fontWeight: 'bold',
    },

    text: {
        fontSize: font.Title3,
        color: color.GrayAAA,
    },

    progressBarContainer: {
        marginTop: 10,
        marginBottom: 10,
        width: '100%',
        height: 22,
        backgroundColor: color.BlueAAA,
        borderRadius: 50,
        justifyContent: 'center'
    },

    innerProgressBarContainer: {
        width: '50%',
        height: 11,
        backgroundColor: color.TintBlue,
        borderRadius: 50,
        marginLeft: 8
    },

    otherSubHeaderTitle: {
        fontSize: 22,
        color: '#404040',
        fontWeight: 'bold',
    },

    monthlyGoalText: {
        fontSize: 18,
        fontWeight: 'bold',
        color: '#404040'
    },
});

export default ProgressInformation;