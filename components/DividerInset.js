import React from 'react';
import { StyleSheet, Text, View } from 'react-native';

const DividerInset = (props) => {
    if (props.type == 'Inset') {
        return (
            <View style={styles.insetCardContainer}>
            </View>
        );
    }
    if (props.type == 'FullWidth') {
        return (
            <View style={styles.fullwidthCardContainer}>
            </View>
        );
    }
    if (props.type == 'Center') {
        return (
            <View style={styles.centerCardContainer}>
            </View>
        );
    }
};

const styles = StyleSheet.create({
    insetCardContainer: {
        marginLeft: 15,
        backgroundColor: '#D3D3D3',
        flex: 1,
        height: 1
    },
    fullwidthCardContainer: {
        backgroundColor: '#D3D3D3',
        flex: 1,
        height: 1
    },
    centerCardContainer: {
        marginLeft: 15,
        marginRight: 15,
        backgroundColor: '#D3D3D3',
        flex: 1,
        height: 1
    },
});

export default DividerInset;