import React from 'react';
import { StyleSheet, Text, View, Image } from 'react-native';
import * as CustomStyles from '../constants/style';
import * as Fonts from '../constants/Font';
import * as Dimensions from '../constants/Dimension';
import * as Color from '../constants/Color';

const font = Fonts.Font;
const dimension = Dimensions.Dimension;
const color = Color.Color;

const Information2Editable = (props) => {
    return (
        <View style={CustomStyles.styles.card}>
            <View style={styles.informationContainer}>
                <Text style={styles.title}>{props.title}</Text>
                <Text style={styles.text}>{props.text}</Text>
                <View style={styles.chevronRightContainer}>
                    <Image
                        source={require('../assets/chevron_right.png')} />
                </View>
            </View>
        </View>
    );
};

const styles = StyleSheet.create({
    informationContainer: {
        padding: dimension.Sixteen
    },

    textContainer: {
        marginBottom: dimension.Eight,
    },

    title: {
        fontSize: font.Title2,
        color: color.GrayAAA,
        fontWeight: 'bold',
    },

    text: {
        fontSize: font.Title3,
        color: color.GrayAAA
    },

    chevronRightContainer: {
        flexDirection: 'column',
        justifyContent: 'center'
    }
});

export default Information2Editable;