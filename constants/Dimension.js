export const Dimension = {
    Sixteen: 16,
    TwentyFour: 24,
    FourtyEight: 48,
    Eight: 8,
    Fourty: 40,
    ThirtyTwo: 32,
    SeventySix: 76
}