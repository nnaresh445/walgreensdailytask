export const Font = {
    LargeTitle: 33,
    Title1: 27,
    Title2: 21,
    Title3: 19,
    Body: 16,
    Callout: 15,
    Subhead: 14,
    Footnote: 12
}
