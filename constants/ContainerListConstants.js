
export let props = {
    header: "PAWS Chicago ",
    subHeader: "What your money will do today to help:",
    message: "Help out the cats and dogs by donating your rewards for enrichment toys!"
}

export let propsII = {
    header: "Center on Halsted ",
    subHeader: "What your money will do today to help:",
    message: "Help to keep fighting for no discrimination among LGBTQIA+ in the workplace."
}

export let propsIII = {
    header: "Greater Chicago Food...",
    subHeader: "What your money will do today to help:",
    message: "Width COVID-19 pandemic, many families are without food, help us provide meals for them"
}

export let propsIV = {
    header: "Human Rights Campaign",
    subHeader: "What your money will do today to help:",
    message: "Basic rights are being stripped down with the current presidency for LGBTQA+ community."
}

export let monthlyGoalI = {
    header: "Our Bi-Monthly Goal",
    message: "50% to our goal for local charities!"
}

export let monthlyGoalII = {
    header: "Our 3 month Goal",
    message: "50% to our goal for local charities!"
}

