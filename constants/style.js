import { StyleSheet } from 'react-native';

export const styles = StyleSheet.create({
    card: {
        shadowColor: 'black',
        shadowOffset: { width: 0, height: 2 },
        shadowRadius: 6,
        shadowOpacity: 0.26,
        padding: 10,
        backgroundColor: 'white',
        elevation: 5,
        borderRadius: 15
    },
});

