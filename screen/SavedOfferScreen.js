import React from 'react';
import { StyleSheet, Text, View, Button, Image, ScrollView, ProgressBarAndroid, ProgressViewIOS, TouchableOpacity, Platform } from 'react-native';
import ScrollableTabView from 'react-native-scrollable-tab-view';
import TabBar from "react-native-underline-tabbar";
import { Color } from '../constants/Color';
import ProductCoupon from '../components/ProductCoupon';

const Page = ({ label, navigation}) => {
    if (label == 'Coupons') {
        return (
            <ScrollView style={styles.scrollView}>
                <View style={styles.homeScreenContainer}>
                    <View style={{ alignItems: 'center', paddingBottom: 40 }}>
                        <View style={styles.toSortContainer}>
                            <View style={styles.toSortSubContainer}>
                                <Text>
                                    <Text style={styles.toSortByText}>Sort by: </Text>
                                    <Text style={styles.toSortText}>Expiring soonest |</Text>
                                </Text>
                            </View>
                        </View>
                    </View>
                    <View style={{ flexDirection: 'row', justifyContent: 'space-evenly', flexWrap: 'wrap' }}>
                        <ProductCoupon bodyTitle='$2.00 off' subText='pantene' detailsText='See details' buttonText='Remove' detailsAction={() => navigation.navigate('NonProfit')} buttonAction={() => navigation.navigate('Success')}>
                        </ProductCoupon>
                        <ProductCoupon bodyTitle='$1.50 off' subText='pantene' detailsText='See details' buttonText='Remove' detailsAction={() => navigation.navigate('NonProfit')} buttonAction={() => navigation.navigate('Success')}>
                        </ProductCoupon>
                    </View>
                </View>
            </ScrollView>
        );
    } else {
        return (
            <View></View>
        );
    }
}

const SavedOffersScreen = ({ navigation }) => {
    return (
        // <ScrollView style={styles.scrollView}>
        // <View style={styles.homeScreenContainer}>
        //     <View style={{alignItems:'center'}}>
        //     <View style={styles.toSortContainer}>
        //         <View style={styles.toSortSubContainer}>
        //             <Text>
        //             <Text style={styles.toSortByText}>Sort by: </Text>
        //             <Text style={styles.toSortText}>Expiring soonest |</Text>
        //             </Text>
        //         </View>
        //     </View>
        //     </View>
        // </View>
        // </ScrollView>
        <View style={[styles.container, { paddingTop: 0 }]}>
            <ScrollableTabView
                tabBarActiveTextColor={Color.BlueAAA}
                renderTabBar={() => <TabBar underlineColor={Color.BlueAAA} />}>
                <Page tabLabel={{ label: 'Coupons' }} label="Coupons" navigation={navigation}/>
                <Page tabLabel={{ label: 'Weekly Ad' }} label="Weekly Ad" />
            </ScrollableTabView>
        </View>
    );
};

const styles = StyleSheet.create({

    homeScreenContainer: {
        flex: 1,
        backgroundColor: '#D3D3D3',
        paddingLeft: 16,
        paddingTop: 16,
        paddingRight: 16,
        paddingBottom: 3
    },

    scrollView: {
        backgroundColor: '#D3D3D3'
    },

    toSortContainer: {
        justifyContent: 'center',
    },

    toSortSubContainer: {
        padding: 5,
        borderRadius: 30,
        paddingLeft: 15,
        paddingRight: 15,
        borderWidth: 2,
        borderColor: '#404040'
    },

    toSortByText: {
        fontSize: 18,
        textAlign: 'center',
        color: '#404040'
    },

    toSortText: {
        fontSize: 18,
        fontWeight: 'bold',
        textAlign: 'center',
        color: '#404040'
    },

    coupons: {
        marginTop: 80
    },


    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#F5FCFF',
    },
});
export default SavedOffersScreen;