import React from 'react';
import { StyleSheet, Text, View, Button, Image, ScrollView, ProgressBarAndroid, ProgressViewIOS, TouchableOpacity, Platform } from 'react-native';
import * as ContainerListConstants from '../constants/ContainerListConstants';
import ContainerListRow from '../components/ContainerListRow';

const NonProfitDetailsScreen = ({ navigation }) => {
    return (
        <ScrollView style={styles.scrollView}>
            <View style={styles.homeScreenContainer}>
                <View style={styles.detailsLogoContainer}>
                    <Image
                        style={styles.detailsLogo}
                        source={require('../assets/walgreens_logo2.png')} />
                </View>

                <Text style={styles.detailsScreenHeaderTitle}> PAWS Chicago </Text>

                <View style={styles.makeAnImpactContainer}>
                    <Text style={styles.detailsSubHeaderTitle}>What your money will do today to help: </Text>
                    <Text style={styles.message}>
                        Help out the cats and dogs by donating your rewards for enrichment toys!
                </Text>
                    <Text style={styles.detailsSubHeaderTitle}>
                        About PAWS:
                </Text>
                    <Text style={styles.message}>
                        This is Chicago's only non-kill shelter blah blah blah
                </Text>
                    <Text style={styles.detailsSubHeaderLinkTitle}>
                        Link to website
                </Text>
                </View>

                <View style={styles.otherCardContainer}>
                    <Text style={styles.otherSubHeaderTitle}>Rewards to donate</Text>
                    <View style={styles.toMainCardContainer}>
                        <View style={styles.toContributeMessageContainerText}>
                            <Text style={styles.toContributeMessageText}>
                                $22.56 available in wallet.
                        </Text>
                        </View>
                        <View style={styles.toContributeMoneyContainer}>
                            <View style={styles.toContributeMoneySubContainer}>
                                <Text style={styles.toContributeMoneyText}>$22.00 |</Text>
                            </View>
                        </View>
                    </View>
                </View>
            </View>
            <View style={styles.bottomButtonContainer}>
                <TouchableOpacity onPress={() => navigation.navigate('Success')}>
                    <View style={styles.buttonView}>
                        <Text style={{ color: '#FFFFFF', fontSize: 20, fontWeight: 'bold' }}>
                            Contribute Walgreens (R) Cash
                                </Text>
                    </View>
                </TouchableOpacity>
            </View>
        </ScrollView>
    );
};

const styles = StyleSheet.create({
    buttonView: {
        backgroundColor: '#696969',
        alignItems: 'center',
        justifyContent: 'center',
        padding: 25,
        borderRadius: 35,
    },

    homeScreenContainer: {
        flex: 1,
        backgroundColor: '#D3D3D3',
        paddingLeft: 16,
        paddingTop: 16,
        paddingRight: 16,
        paddingBottom: 3
    },
    
    bottomButtonContainer: {
        flex: 1,
        backgroundColor: '#FFFFFF',
        padding: 16
    },

    makeAnImpactContainer: {
        shadowColor: 'black',
        shadowOffset: { width: 0, height: 2 },
        shadowRadius: 6,
        shadowOpacity: 0.26,
        padding: 20,
        backgroundColor: 'white',
        elevation: 5,
        borderRadius: 10,
        marginBottom: 50
    },

    otherCardContainer: {
        shadowColor: 'black',
        shadowOffset: { width: 0, height: 2 },
        shadowRadius: 6,
        shadowOpacity: 0.26,
        padding: 15,
        backgroundColor: 'white',
        elevation: 5,
        borderRadius: 15,
        marginBottom: 10
    },

    detailsSubHeaderTitle: {
        fontSize: 18,
        color: '#404040',
        fontWeight: 'bold',
    },
    detailsSubHeaderLinkTitle: {
        fontSize: 18,
        color: '#696969',
        fontWeight: 'bold',
    },

    message: {
        fontSize: 18,
        color: '#404040',
        paddingBottom: 30
    },
    scrollView: {
        backgroundColor: '#D3D3D3'
    },

    otherSubHeaderTitle: {
        fontSize: 22,
        color: '#404040',
        fontWeight: 'bold',
    },

    detailsScreenHeaderTitle: {
        justifyContent: 'center',
        alignSelf: 'center',
        fontSize: 30,
        color: '#404040',
        fontWeight: 'bold',
        marginTop: 30,
        marginBottom: 30
    },

    toContributeMessageContainerText: {
        flex: 1
    },
    toMainCardContainer: {
        flexDirection: 'row',
        flex: 1,
        justifyContent: 'space-between',
    },

    toContributeMessageText: {
        flexWrap: 'wrap',
        fontSize: 18,
        color: '#404040',
        paddingRight: 10
    },

    toContributeMoneyContainer: {
        paddingLeft: 10,
    },

    toContributeMoneySubContainer: {
        padding: 5,
        borderRadius: 30,
        borderWidth: 2,
        borderColor: '#404040'
    },

    toContributeMoneyText: {
        fontSize: 18,
        paddingRight: 10,
        paddingLeft: 10,
        fontWeight: 'bold',
        textAlign: 'center',
        color: '#404040'

    },

    detailsLogoContainer: {
        justifyContent: 'center',
        alignSelf: 'center',
        borderRadius: 75,
        borderWidth: 0,
        shadowColor: 'black',
        shadowOffset: { width: 1, height: 4 },
        shadowRadius: 3,
        shadowOpacity: 0.4,
        elevation: 1,
        ...Platform.select({
            ios: {
                borderWidth: 1,
            }
        })

    },

    detailsLogo: {
        width: 150,
        height: 150
    },
});
export default NonProfitDetailsScreen;