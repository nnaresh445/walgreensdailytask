import React from 'react';
import { StyleSheet, Text, View, Button } from 'react-native';

const HomeScreen = ({ navigation }) => {
    return (
        <View style={styles.homeScreenContainer}>
            <Text>Home Screen</Text>
            <Button
                color="#ff0000"
                title="Goto Today's Task"
                onPress={() => navigation.navigate('Task')}
            />
            <Button
                title="Goto SecondScreen"
                onPress={() => navigation.navigate('Second')}
            />
            <Button
                color="#ff0000"
                title="Goto ThirdScreen"
                onPress={() => navigation.navigate('Third')}
            />
            <Button
                color="#ff0000"
                title="Goto NonProfit Detail Screen"
                onPress={() => navigation.navigate('NonProfit')}
            />
            <Button
                color="#ff0000"
                title="Goto Success Screen"
                onPress={() => navigation.navigate('Success')}
            /> 
            <Button
                color="#ff0000"
                title="Goto WalletScreen Screen"
                onPress={() => navigation.navigate('WalletScreen')}
            /> 
            <Button
                color="#ff0000"
                title="Goto Components Demo Screen"
                onPress={() => navigation.navigate('Components')}
            /> 
            <Button
                color="#ff0000"
                title="Goto Saved offers Screen"
                onPress={() => navigation.navigate('SavedOffersScreen')}
            /> 
        </View>
    );
};

const styles = StyleSheet.create({
    homeScreenContainer: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: '#f0ffff',
        padding: 16
    }
});

export default HomeScreen;