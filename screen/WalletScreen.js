import React from 'react';
import { StyleSheet, Text, View, Button, Image, ScrollView, ProgressBarAndroid, ProgressViewIOS, TouchableOpacity, Platform } from 'react-native';
import Information2Editable from '../components/Information2Editable';
import DividerInset from '../components/DividerInset';
import { Color } from '../constants/Color';

const WalletScreen = ({ navigation }) => {
    return (
        <ScrollView style={styles.scrollView}>
            <View style={{
                flex: 1,
                flexDirection: 'column',
                justifyContent: 'space-between',
            }}>
                <View style={styles.homeScreenContainer}>

                    <View style={{ flexDirection: 'row', marginBottom: 40 }}>
                        <View style={{ justifyContent: 'center', alignItems: 'flex-start', marginRight: 10 }}>
                            <Image style={{ width: 50, height: 50 }}
                                source={require('../assets/walgreens_logo2.png')} />
                        </View>
                        <View style={{ alignItems: 'flex-start' }}>
                            <Text style={{ fontSize: 28, fontWeight:'bold', color:'#FFFFFF' }}>
                                Wallet
                        </Text>
                        </View>
                    </View>

                    <Text style={styles.message}>
                        Please review your payment information. When you're ready to pay, tap continue
                </Text>
                    <View style={styles.makeAnImpactContainer}>
                        <TouchableOpacity onPress={() => navigation.navigate('SavedOffersScreen')}>
                        <View style={{ flexDirection: 'row', justifyContent: 'space-between', padding: 15 }}>
                            <View>
                                <Text style={{ fontSize: 21 }}>
                                    Saved offer
                        </Text>
                                <Text style={{ fontSize: 19 }}>
                                    x clipped
                        </Text>
                            </View>
                            <View style={{ justifyContent: 'center', alignItems: 'flex-end' }}>
                                <Image style={{ width: 30, height: 30 }}
                                    source={require('../assets/chevron_right.png')} />
                            </View>
                        </View>
                        </TouchableOpacity>
                        <DividerInset type='Inset'></DividerInset>
                        <View style={{ flexDirection: 'row', justifyContent: 'space-between', padding: 15 }}>
                            <View>
                                <Text style={{ fontSize: 21 }}>
                                    Use Walgreens cash
                        </Text>
                                <Text style={{ fontSize: 19 }}>
                                    $10.17 in rewards available
                        </Text>
                            </View>
                            <View style={{ justifyContent: 'center', alignItems: 'flex-end' }}>
                                <View style={styles.toContributeMoneyContainer}>
                                    <View style={styles.toContributeMoneySubContainer}>
                                        <Text style={styles.toContributeMoneyText}>$1</Text>
                                    </View>
                                </View>
                            </View>
                        </View>
                        <DividerInset type='Inset'></DividerInset>
                        <View style={{ flexDirection: 'row', padding: 15, justifyContent: 'space-between', }}>
                            <View style={{ flexDirection: 'row' }}>
                                <View style={{ justifyContent: 'center', alignItems: 'flex-start', marginRight: 10 }}>
                                    <Image style={{ width: 50, height: 50 }}
                                        source={require('../assets/creditCard.png')} />
                                </View>
                                <View style={{ alignItems: 'flex-start' }}>
                                    <Text style={{ fontSize: 21 }}>
                                        Payment
                        </Text>
                                    <Text style={{ fontSize: 19 }}>
                                        Credit card *0614
                        </Text>
                                </View>
                            </View>
                            <View style={{ justifyContent: 'center', alignItems: 'flex-end' }}>
                                <Image style={{ width: 30, height: 30 }}
                                    source={require('../assets/chevron_right.png')} />
                            </View>
                        </View>
                    </View>
                </View>
                <View style={styles.bottomButtonContainer}>
                    <TouchableOpacity onPress={() => navigation.navigate('Success')}>
                        <View style={styles.buttonView}>
                            <Text style={{ color: '#FFFFFF', fontSize: 20, fontWeight: 'bold' }}>
                                Continue
                                </Text>
                        </View>
                    </TouchableOpacity>
                </View>
            </View>
        </ScrollView>
    );
};

const styles = StyleSheet.create({
    buttonView: {
        backgroundColor: Color.GrayAAA,
        alignItems: 'center',
        justifyContent: 'center',
        padding: 15,
        borderRadius: 35,
    },

    homeScreenContainer: {
        flex: 1,
        backgroundColor: Color.GrayAAA,
        paddingLeft: 16,
        paddingTop: 16,
        paddingRight: 16,
        paddingBottom: 3
    },

    bottomButtonContainer: {
        flex: 1,
        backgroundColor: '#FFFFFF',
        padding: 16,
    },

    makeAnImpactContainer: {
        shadowColor: 'black',
        shadowOffset: { width: 0, height: 2 },
        shadowRadius: 6,
        shadowOpacity: 0.26,
        backgroundColor: 'white',
        elevation: 5,
        borderRadius: 10,
        marginBottom: 50
    },

    otherCardContainer: {
        shadowColor: 'black',
        shadowOffset: { width: 0, height: 2 },
        shadowRadius: 6,
        shadowOpacity: 0.26,
        padding: 15,
        backgroundColor: 'white',
        elevation: 5,
        borderRadius: 15,
        marginBottom: 10
    },

    detailsSubHeaderTitle: {
        fontSize: 18,
        color: '#404040',
        fontWeight: 'bold',
    },
    detailsSubHeaderLinkTitle: {
        fontSize: 18,
        color: '#696969',
        fontWeight: 'bold',
    },

    message: {
        fontSize: 22,
        color: '#FFFFFF',
        paddingBottom: 50
    },
    scrollView: {
        backgroundColor: '#FFFFFF'
    },

    otherSubHeaderTitle: {
        fontSize: 22,
        color: '#404040',
        fontWeight: 'bold',
    },

    detailsScreenHeaderTitle: {
        justifyContent: 'center',
        alignSelf: 'center',
        fontSize: 30,
        color: '#404040',
        fontWeight: 'bold',
        marginTop: 30,
        marginBottom: 30
    },

    toContributeMessageContainerText: {
        flex: 1
    },
    toMainCardContainer: {
        flexDirection: 'row',
        flex: 1,
        justifyContent: 'space-between',
    },

    toContributeMessageText: {
        flexWrap: 'wrap',
        fontSize: 18,
        color: '#404040',
        paddingRight: 10
    },

    toContributeMoneyContainer: {
        paddingLeft: 10,
    },

    toContributeMoneySubContainer: {
        padding: 5,
        borderRadius: 30,
        borderWidth: 2,
        borderColor: '#404040'
    },

    toContributeMoneyText: {
        fontSize: 18,
        paddingRight: 20,
        paddingLeft: 20,
        fontWeight: 'bold',
        textAlign: 'center',
        color: '#404040'

    },

    detailsLogoContainer: {
        justifyContent: 'center',
        alignSelf: 'center',
        borderRadius: 75,
        borderWidth: 0,
        shadowColor: 'black',
        shadowOffset: { width: 1, height: 4 },
        shadowRadius: 3,
        shadowOpacity: 0.4,
        elevation: 1,
        ...Platform.select({
            ios: {
                borderWidth: 1,
            }
        })

    },

    detailsLogo: {
        width: 150,
        height: 150
    },
});
export default WalletScreen;