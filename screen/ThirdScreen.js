import React from 'react';
import { StyleSheet, Text, View, Button, Image, ScrollView, ProgressBarAndroid, ProgressViewIOS, TouchableOpacity, Platform } from 'react-native';
import * as ContainerListConstants from '../constants/ContainerListConstants';
import ContainerListRow from '../components/ContainerListRow';
import DividerInset from '../components/DividerInset';

const dividerType = { 
    Inset: 'Inset',
    FullWidth: 'FullWidth',
    Center: 'Center'
};

const ThirdScreen = () => {
    return (
        <ScrollView style={styles.scrollView}>
            <View style={styles.homeScreenContainer}>
                <Text style={styles.otherHeaderTitle}> Walgreens Cash to give </Text>
                <View style={styles.otherCardContainer}>
                    <Text style={styles.otherSubHeaderTitle}>To contribute</Text>
                    <View style={styles.toMainCardContainer}>
                        <View style={styles.toContributeMessageContainerText}>
                            <Text style={styles.toContributeMessageText}>
                                You have $22.71 in Walgreens(R) Cash Rewards to give.
                        </Text>
                        </View>
                        <View style={styles.toContributeMoneyContainer}>
                            <View style={styles.toContributeMoneySubContainer}>
                                <Text style={styles.toContributeMoneyText}>$22.00</Text>
                            </View>
                        </View>
                    </View>
                </View>
                <View style={styles.otherCardContainer}>
                    <View style={styles.toMainCardContainer}>
                        <View style={styles.infoButtonContainer}>
                            <TouchableOpacity onPress={() => { }}>
                                <Image style={{ height: 40, width: 40, justifyContent: 'center' }}
                                    source={require('../assets/help.png')} />
                            </TouchableOpacity>
                        </View>
                        <Text style={{ alignItems: 'center', paddingTop: 10, paddingLeft: 10, fontSize: 18 }}>
                            How contributing walgreens cash works.
                        </Text>
                    </View>
                </View>
                <Text style={styles.otherHeaderTitle}> Your Impact </Text>
                <View style={styles.yourImpactCardContainer} >
                    <View style={{ flex: 1, padding: 15, borderBottomLeftRadius: 15, borderBottomRightRadius: 15, overflow: 'hidden' }} >
                        <Text style={styles.otherHeaderTitle}> With your help </Text>
                        <DividerInset type='Center'></DividerInset>
                        <Text style={{ paddingLeft: 5, fontSize: 18, paddingBottom: 20 }}>
                            We've funded $XXXXX to local communities within the area.
                            </Text>
                    </View>
                </View>

                <Text style={styles.otherHeaderTitle}>National Causes </Text>
                <View style={styles.otherCardContainer}>
                    <Text style={styles.otherSubHeaderTitle}>Monthly Goal </Text>
                    <View style={styles.progressBarContainer}>
                        {Platform.OS == 'android' ?
                            <ProgressBarAndroid
                                styleAttr="Horizontal"
                                indeterminate={false}
                                progress={0.5}
                            />
                            : <ProgressViewIOS
                                style={styles.progress}
                                progressTintColor=""
                                progress={0.5}
                            />
                        }
                    </View>
                    <Text style={styles.monthlyGoalText}>50% to our goal for local charities! </Text>
                </View>
                <View style={styles.listCardContainer}>
                    <ContainerListRow {...ContainerListConstants.propsIV} />
                    <View style={styles.progressBarContainerII}>
                        {Platform.OS == 'android' ?
                            <ProgressBarAndroid
                                styleAttr="Horizontal"
                                indeterminate={false}
                                progress={0.5}
                            />
                            : <ProgressViewIOS
                                style={styles.progress}
                                progressTintColor=""
                                progress={0.5}
                            />
                        }
                    </View>
                    <View style={styles.monthlyGoalTextContainer}>
                        <Text style={styles.monthlyGoalText}>Halfway to their goal!</Text>
                    </View>
                </View>

                <Text style={styles.otherHeaderTitle}> Local Causes </Text>
                <View style={styles.currentLocationContainer}>
                    <Text style={styles.currentLocationText}>Current location: </Text>
                    <Text>60301</Text>
                    <Image
                        source={require('../assets/down_arrow.png')} />
                </View>
                <View style={styles.otherCardContainer}>
                    <Text style={styles.otherSubHeaderTitle}>Monthly Goal </Text>
                    <View style={styles.progressBarContainer}>
                        {Platform.OS == 'android' ?
                            <ProgressBarAndroid
                                styleAttr="Horizontal"
                                indeterminate={false}
                                progress={0.5}
                            />
                            : <ProgressViewIOS
                                style={styles.progress}
                                progressTintColor=""
                                progress={0.5}
                            />
                        }
                    </View>
                    <Text style={styles.monthlyGoalText}>50% to our goal for local charities! </Text>
                </View>
                <View style={styles.listCardContainer}>
                    <ContainerListRow {...ContainerListConstants.props} />
                </View>
                <View style={styles.listCardContainer}>
                    <ContainerListRow {...ContainerListConstants.propsII} />
                </View>
                <View style={styles.listCardContainer}>
                    <ContainerListRow {...ContainerListConstants.propsIII} />
                </View>
            </View>
        </ScrollView>
    );
};

const styles = StyleSheet.create({
    infoButtonContainer: {
        backgroundColor: '#fff',
        alignItems: 'center',
        justifyContent: 'center',
    },

    homeScreenContainer: {
        flex: 1,
        backgroundColor: '#f0ffff',
        padding: 16
    },

    otherCardContainer: {
        shadowColor: 'black',
        shadowOffset: { width: 0, height: 2 },
        shadowRadius: 6,
        shadowOpacity: 0.26,
        padding: 15,
        backgroundColor: 'white',
        elevation: 5,
        borderRadius: 15,
        marginBottom: 10
    },

    yourImpactCardContainer: {
        shadowColor: 'black',
        shadowOffset: { width: 0, height: 2 },
        shadowRadius: 6,
        shadowOpacity: 0.26,
        backgroundColor: 'white',
        elevation: 5,
        borderRadius: 15,
    },

    subHeaderTitle: {
        fontSize: 22,
        color: '#404040',
        fontWeight: 'bold',
        textAlign: 'center',
        marginTop: 15
    },

    scrollView: {
        backgroundColor: '#f0ffff'
    },

    otherSubHeaderTitle: {
        fontSize: 22,
        color: '#404040',
        fontWeight: 'bold',
    },

    otherHeaderTitle: {
        fontSize: 30,
        color: '#404040',
        fontWeight: 'bold',
        marginTop: 15,
        marginBottom: 10
    },

    toContributeMessageContainerText: {
        flex: 1
    },

    toMainCardContainer: {
        flexDirection: 'row',
        flex: 1,
        justifyContent: 'space-between',
    },

    toContributeMessageText: {
        flexWrap: 'wrap',
        fontSize: 18,
        color: '#404040',
        paddingRight: 10
    },

    toContributeMoneyContainer: {
        paddingLeft: 10,
    },

    toContributeMoneySubContainer: {
        padding: 5,
        borderRadius: 30,
        borderWidth: 2,
        borderColor: '#404040'
    },

    toContributeMoneyText: {
        fontSize: 18,
        paddingRight: 10,
        paddingLeft: 10,
        fontWeight: 'bold',
        textAlign: 'center',
        color: '#404040'

    },

    currentLocationContainer: {
        flexDirection: 'row',
        borderRadius: 25,
        width: 250,
        alignContent: 'center',
        justifyContent: 'space-between',
        alignItems: 'center',
        borderWidth: 1,
        marginBottom: 20
    },

    currentLocationText: {
        fontSize: 18,
        fontWeight: 'bold',
        color: '#404040',
        paddingLeft: 15
    },

    monthlyGoalText: {
        fontSize: 18,
        fontWeight: 'bold',
        color: '#404040'
    },

    monthlyGoalTextContainer: {
        marginLeft: 80
    },

    progressBarContainer: {
        marginTop: 10,
        marginBottom: 10
    },

    progressBarContainerII: {
        marginTop: 10,
        marginBottom: 10,
        width: 150,
        marginLeft: 80
    },

    listCardContainer: {
        shadowColor: 'black',
        shadowOffset: { width: 0, height: 2 },
        shadowRadius: 6,
        shadowOpacity: 0.26,
        padding: 20,
        backgroundColor: 'white',
        elevation: 5,
        borderRadius: 15,
        marginTop: 15,
        marginBottom: 10
    },
});

export default ThirdScreen;