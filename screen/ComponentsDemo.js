import React from 'react';
import { StyleSheet, Text, View, Button, Image, ScrollView, ProgressBarAndroid, ProgressViewIOS, TouchableOpacity, Platform } from 'react-native';
import * as ContainerListConstants from '../constants/ContainerListConstants';
import ContainerListRow from '../components/ContainerListRow';
import ProductCoupon from '../components/ProductCoupon';

const ComponentsDemo = ({ navigation }) => {
    return (
        <ScrollView style={styles.scrollView}>
            <View style={styles.homeScreenContainer}>
                <ProductCoupon bodyTitle='Text' subText='Some'></ProductCoupon>
            </View>
        </ScrollView>
    );
};

const styles = StyleSheet.create({
    buttonView: {
        backgroundColor: '#696969',
        alignItems: 'center',
        justifyContent: 'center',
        padding: 20,
        borderRadius: 35,
    },

    shareButtonView: {
        backgroundColor: '#696969',
        alignItems: 'center',
        justifyContent: 'center',
        flexDirection: 'row',
        padding: 10,
        borderRadius: 20,
    },

    homeScreenContainer: {
        flex: 1,
        backgroundColor: '#D3D3D3',
        paddingLeft: 16,
        paddingTop: 16,
        paddingRight: 16,
        paddingBottom: 3
    },

    shareButtonContainer: {
        flex: 1,
        margin: 5
    },

    bottomButtonContainer: {
        flex: 1,
        marginTop: 20,
        padding: 16
    },

    message: {
        fontSize: 18,
        color: '#404040',
        paddingBottom: 10
    },

    scrollView: {
        backgroundColor: '#D3D3D3'
    },

    successScreenHeaderTitle: {
        fontSize: 30,
        color: '#404040',
        fontWeight: 'bold',
        marginTop: 10,
        marginBottom: 10
    },

    detailsLogoContainer: {
        justifyContent: 'center',
        alignSelf: 'center',
        elevation: 1,
    },

    shareLogoContainer: {
        justifyContent: 'center',
        alignSelf: 'center',
        elevation: 1,
        paddingRight: 10
    },

    detailsLogo: {
        width: 60,
        height: 60
    },

    shareBuutonLogo: {
        width: 20,
        height: 20,
        paddingLeft: 10
    },
});
export default ComponentsDemo;