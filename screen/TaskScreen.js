import React from 'react';
import { StyleSheet, Text, View, Button, Image, ScrollView, ProgressBarAndroid, ProgressViewIOS, TouchableOpacity, Platform } from 'react-native';
import * as ContainerListConstants from '../constants/ContainerListConstants';
import ContainerListRow from '../components/ContainerListRow';
import * as Color from '../constants/Color';
import * as Dimension from '../constants/Dimension';
import ProgressInformation from '../components/ProgressInformation'

const Colors = Color.Color;
const dimension = Dimension.Dimension;

const TaskScreen = ({ navigation }) => {
    return (
        <ScrollView style={styles.scrollView}>
            <View style={styles.homeScreenContainer}>
                <View style={styles.makeAnImpactContainer}> 
                    <View style={styles.headerTitleContainer}>
                        <Text style={styles.headerTitle}>Make an impact today</Text>
                        <Image style={styles.close}
                            source={require('../assets/cancel_button.png')} />
                    </View>
                    <Text style={styles.subHeaderTitle}>How it works</Text>
                    <Text style={styles.message}>
                        Give your Walgreens Cash (TM) Rewards to an non profit of your choice. you'll earn on every time you shop, you can contribute any amount you'd like. It's that easy.
                </Text>
                    <Text style={styles.subHeaderTitle}>
                        Every dollar counts
                </Text>
                    <Text style={styles.message}>
                        When ever you'd like to contribute $1, $5, or more, you will be making a big difference. It really adds up!
                </Text>
                </View>

                <Text style={styles.otherHeaderTitle}> Walgreens Cash to give </Text>
                <View style={styles.otherCardContainer}>
                    <Text style={styles.otherSubHeaderTitle}>To contribute</Text>
                    <View style={styles.toMainCardContainer}>
                        <View style={styles.toContributeMessageContainerText}>
                            <Text style={styles.toContributeMessageText}>
                                You have $22.71 in Walgreens(R) Cash Rewards to give.
                        </Text>
                        </View>
                        <View style={styles.toContributeMoneyContainer}>
                            <View style={styles.toContributeMoneySubContainer}>
                                <Text style={styles.toContributeMoneyText}>$22.00</Text>
                            </View>
                        </View>
                    </View>
                </View>

                <Text style={styles.otherHeaderTitle}>National Causes </Text>
                <ProgressInformation {...ContainerListConstants.monthlyGoalI} />
                <View style={styles.listCardContainer}>
                    <ContainerListRow {...ContainerListConstants.propsIV} />
                </View>
                <Text style={styles.otherHeaderTitle}> Local Causes </Text>
                <View style={styles.currentLocationContainer}>
                    <Text style={styles.currentLocationText}>Current location: </Text>
                    <Text>60301</Text>
                    <Image
                        source={require('../assets/down_arrow.png')} />
                </View>
                <ProgressInformation {...ContainerListConstants.monthlyGoalII} />
                <TouchableOpacity onPress={() => navigation.navigate('NonProfit')}>
                    <View style={styles.listCardContainer}>
                        <ContainerListRow {...ContainerListConstants.props} />
                    </View>
                </TouchableOpacity>
                <TouchableOpacity onPress={() => navigation.navigate('NonProfit')}>
                    <View style={styles.listCardContainer}>
                        <ContainerListRow {...ContainerListConstants.propsII} />
                    </View>
                </TouchableOpacity>
                <TouchableOpacity onPress={() => navigation.navigate('NonProfit')}>
                    <View style={styles.listCardContainer}>
                        <ContainerListRow {...ContainerListConstants.propsIII} />
                    </View>
                </TouchableOpacity>
            </View>
        </ScrollView>
    );
};

const styles = StyleSheet.create({
    homeScreenContainer: {
        flex: 1,
        backgroundColor: '#f0ffff',
        padding: dimension.Sixteen
    },

    close: {
        margin: 5,
        position: "absolute",
        top: 0,
        right: 0,
    },

    makeAnImpactContainer: {
        shadowColor: 'black',
        shadowOffset: { width: 0, height: 2 },
        shadowRadius: 6,
        shadowOpacity: 0.26,
        padding: 10,
        backgroundColor: 'white',
        elevation: 5,
        borderRadius: 15
    },

    otherCardContainer: {
        shadowColor: 'black',
        shadowOffset: { width: 0, height: 2 },
        shadowRadius: 6,
        shadowOpacity: 0.26,
        padding: 15,
        backgroundColor: 'white',
        elevation: 5,
        borderRadius: 15,
        marginBottom: 10
    },

    headerTitleContainer: {
        flexDirection: 'row',
        justifyContent: 'space-between'
    },

    headerTitle: {
        fontSize: 25,
        color: '#404040',
        fontWeight: 'bold'
    },

    subHeaderTitle: {
        fontSize: 22,
        color: '#404040',
        fontWeight: 'bold',
        textAlign: 'center',
        marginTop: 15
    },

    messageContainer: {
        borderColor: '#404040',
        borderWidth: 1,
        borderRadius: 15,
        marginTop: 10,
        marginBottom: 25,
        marginLeft: 10,
        marginRight: 10,
        paddingTop: 5,
        paddingBottom: 10
    },

    message: {
        fontSize: 20,
        color: '#404040',
        textAlign: 'center',
        marginLeft: 25,
        marginRight: 25
    },

    scrollView: {
        backgroundColor: '#f0ffff'
    },

    otherSubHeaderTitle: {
        fontSize: 22,
        color: '#404040',
        fontWeight: 'bold',
    },

    otherHeaderTitle: {
        fontSize: 30,
        color: '#404040',
        fontWeight: 'bold',
        marginTop: 15,
        marginBottom: 10
    },

    toContributeMessageContainerText: {
        flex: 1
    },

    toMainCardContainer: {
        flexDirection: 'row',
        flex: 1,
        justifyContent: 'space-between',
    },

    toContributeMessageText: {
        flexWrap: 'wrap',
        fontSize: 18,
        color: '#404040',
        paddingRight: 10
    },

    toContributeMoneyContainer: {
        paddingLeft: 10,
    },

    toContributeMoneySubContainer: {
        padding: 5,
        borderRadius: 30,
        borderWidth: 2,
        borderColor: '#404040'
    },

    toContributeMoneyText: {
        fontSize: 18,
        paddingRight: 10,
        paddingLeft: 10,
        fontWeight: 'bold',
        textAlign: 'center',
        color: '#404040'

    },

    currentLocationContainer: {
        flexDirection: 'row',
        borderRadius: 25,
        width: 250,
        alignContent: 'center',
        justifyContent: 'space-between',
        alignItems: 'center',
        borderWidth: 1,
        marginBottom: 20
    },

    currentLocationText: {
        fontSize: 18,
        fontWeight: 'bold',
        color: '#404040',
        paddingLeft: 15
    },

    monthlyGoalText: {
        fontSize: 18,
        fontWeight: 'bold',
        color: '#404040'
    },

    monthlyGoalTextContainer: {
        marginLeft: 80
    },

    progressBarContainer: {
        marginTop: 10,
        marginBottom: 10
    },

    progressBarContainerII: {
        marginTop: 10,
        marginBottom: 10,
        width: 150,
        marginLeft: 80
    },

    listCardContainer: {
        shadowColor: 'black',
        shadowOffset: { width: 0, height: 2 },
        shadowRadius: 6,
        shadowOpacity: 0.26,
        padding: 20,
        backgroundColor: 'white',
        elevation: 5,
        borderRadius: 15,
        marginTop: 15,
        marginBottom: 10
    },
});

export default TaskScreen;